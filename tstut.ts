
let myName: string = "Will";
let myAge: number = 24;
let canVote: boolean = true;
let anything: any = false;
anything = 2;

document.getElementById("tsStuff").innerHTML = "My name is " + myName;

document.write("myName is a " + typeof(myName) + "<br/>");

document.write("myAge is a " + typeof(myAge) + "<br/>");

document.write("canVote is a " + typeof(canVote) + "<br/>");

document.write("anything is a " + typeof(anything) + "<br/>");

var strToNum: number = parseInt("101");

var numToStr: number = 5;
document.write("numToStr is a " + typeof(numToStr.toString()) + "<br/>");

const PI = 3.14;

var employees: string[] = ["Max", "Chloe", "Rachel"];

document.write(employees.toString() + "<br/>");

document.write("5 + 4 = " + (5 + 4) + "<br/>");
document.write("5 - 4 = " + (5 - 4) + "<br/>");
document.write("5 * 4 = " + (5 * 4) + "<br/>");
document.write("5 / 4 = " + (5 / 4) + "<br/>");
document.write("5 % 4 = " + (5 % 4) + "<br/>");

// Arrays
var numArray = [5, 6, 7, 8];
for (var index in numArray) {
    document.write(numArray[index] + ', ');
}
document.write('<br/>');
var strArray = numArray.map(String);
for (var str in strArray) {
    document.write(str + ", ");
}
document.write('<br/>');

// Functions
var getSum = function(num1: number, num2: number): number {
    return num1 + num2;
}
var theSum: number = getSum(5, 2);
document.write('5 + 2 = ' + theSum + '<br/>');

var getDiff = function(num1: number, num2: number, num3?: number): number { // num3 is optional
    if (typeof num3 !== 'undefined') {
        return num1 - num2 - num3;
    }
    return num1 - num2;
}
document.write('5 - 2 = ' + this.getDiff(5, 2) + '<br/>');
document.write('5 - 2 - 3 = ' + this.getDiff(5, 2, 3) + '<br/>');

// variable number of parameters
var sumAll = (...nums: number[]): number => {
    return nums.reduce((a, b) => a + b, 0);
}
document.write('Sum = ' + this.sumAll(8, 6, 7, 5, 3, 0, 9) + '<br/>');

var myValues = {x: 1, y: 2, z: 3};
var {x, y, z} = myValues;
document.write('MyValues: ' + x + y + z + '<br/>');
[x, y, z] = [z, y, x];
document.write('Switch: ' + x + y + z + '<br/>');

var multStr = `I go on for
many lines<br/>`;
document.write(multStr);