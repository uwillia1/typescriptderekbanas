var Car = /** @class */ (function () {
    function Car(wheels) {
        this.wheels = wheels;
    }
    Car.prototype.drive = function () {
        document.write('The car drives with ' + this.wheels + ' wheels <br/>');
    };
    return Car;
}());
var Bicycle = /** @class */ (function () {
    function Bicycle(wheels) {
        this.wheels = wheels;
    }
    Bicycle.prototype.drive = function () {
        document.write('The bicycle drives with ' + this.wheels + ' wheels <br/>');
    };
    return Bicycle;
}());
var car = new Car(4);
var bike = new Bicycle(2);
//car.drive();
//bike.drive();
function getWheels(veh) {
    return veh.drive();
}
this.getWheels(this.car);
this.getWheels(this.bike);
