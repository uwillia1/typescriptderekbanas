
class Animal {
    public favFood: string;
    static numOfAnimals: number = 0;

    constructor(private name: string, private owner: string) {
        Animal.numOfAnimals++;
    }

    printOwnerInfo() {
        document.write(this.name + ' is owned by ' + this.owner + '<br/>');
    }

    static getNumberOfAnimals(): number {
        return Animal.numOfAnimals;
    }

    private _weight: number;

    get weight(): number {
        return this._weight;
    }

    set weight(weight: number) { // setters cannot have any return type; not even void
        this._weight = weight;
    }

}

var spot = new Animal("Spot", "Doug");
spot.printOwnerInfo();
document.write('Number of animals: ' + Animal.getNumberOfAnimals() + '<br/>');

class Dog extends Animal {
    constructor(name: string, owner: string) {
        super(name, owner);
        Dog.numOfAnimals++;
    }
}

var grover = new Dog('Grover', 'Jimmy');
grover.printOwnerInfo();
document.write('Number of animals: ' + Animal.getNumberOfAnimals() + '<br/>');

document.write('Is a Dog an Animal? ' + (grover instanceof Animal) + '<br/>');

class GenericNumber<T> {
    add: (val1: T, val2: T) => T;
}
var aNumber = new GenericNumber<number>();
aNumber.add = function(x, y) {
    return x + y;
}