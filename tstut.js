var myName = "Will";
var myAge = 24;
var canVote = true;
var anything = false;
anything = 2;
document.getElementById("tsStuff").innerHTML = "My name is " + myName;
document.write("myName is a " + typeof (myName) + "<br/>");
document.write("myAge is a " + typeof (myAge) + "<br/>");
document.write("canVote is a " + typeof (canVote) + "<br/>");
document.write("anything is a " + typeof (anything) + "<br/>");
var strToNum = parseInt("101");
var numToStr = 5;
document.write("numToStr is a " + typeof (numToStr.toString()) + "<br/>");
var PI = 3.14;
var employees = ["Max", "Chloe", "Rachel"];
document.write(employees.toString() + "<br/>");
document.write("5 + 4 = " + (5 + 4) + "<br/>");
document.write("5 - 4 = " + (5 - 4) + "<br/>");
document.write("5 * 4 = " + (5 * 4) + "<br/>");
document.write("5 / 4 = " + (5 / 4) + "<br/>");
document.write("5 % 4 = " + (5 % 4) + "<br/>");
// Arrays
var numArray = [5, 6, 7, 8];
for (var index in numArray) {
    document.write(numArray[index] + ', ');
}
document.write('<br/>');
var strArray = numArray.map(String);
for (var str in strArray) {
    document.write(str + ", ");
}
document.write('<br/>');
// Functions
var getSum = function (num1, num2) {
    return num1 + num2;
};
var theSum = getSum(5, 2);
document.write('5 + 2 = ' + theSum + '<br/>');
var getDiff = function (num1, num2, num3) {
    if (typeof num3 !== 'undefined') {
        return num1 - num2 - num3;
    }
    return num1 - num2;
};
document.write('5 - 2 = ' + this.getDiff(5, 2) + '<br/>');
document.write('5 - 2 - 3 = ' + this.getDiff(5, 2, 3) + '<br/>');
// variable number of parameters
var sumAll = function () {
    var nums = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        nums[_i] = arguments[_i];
    }
    return nums.reduce(function (a, b) { return a + b; }, 0);
};
document.write('Sum = ' + this.sumAll(8, 6, 7, 5, 3, 0, 9) + '<br/>');
var myValues = { x: 1, y: 2, z: 3 };
var x = myValues.x, y = myValues.y, z = myValues.z;
document.write('MyValues: ' + x + y + z + '<br/>');
_a = [z, y, x], x = _a[0], y = _a[1], z = _a[2];
document.write('Switch: ' + x + y + z + '<br/>');
var multStr = "I go on for\nmany lines<br/>";
document.write(multStr);
var _a;
