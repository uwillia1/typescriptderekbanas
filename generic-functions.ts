
function getType<T>(val: T): string {
    return typeof(val);
}

let aStr = "I am a string";
let aNum = 22;

document.write('Str is a ' + this.getType(aStr) + '<br/>');
document.write('Num is a ' + this.getType(aNum) + '<br/>');